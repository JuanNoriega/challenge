package org.noriega.Challenge;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReservationController {
	
	@Autowired
	private ReservationService reservationService;

	@RequestMapping(value = "/reservations", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Reservation> getAllReservations(){
		return reservationService.getAllReservations();
	}
	
}
