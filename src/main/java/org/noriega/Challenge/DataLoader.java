package org.noriega.Challenge;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements CommandLineRunner{

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private ReservationService reservationService;
	
	public DataLoader(ReservationService reservationService) {
		this.reservationService = reservationService;
	}
	
	@Override
	public void run(String... args) throws Exception {
		logger.info("Loading sample data...");
		reservationService.addReservation(new Reservation(1, "Juan", LocalDateTime.now()));
		reservationService.addReservation(new Reservation(2, "Luis", LocalDateTime.now()));
		reservationService.addReservation(new Reservation(3, "Jose", LocalDateTime.now()));
		reservationService.addReservation(new Reservation(4, "Maria", LocalDateTime.now()));
	}
}
